### Desafio para vaga de Developer Jr na Trílogo

#### O desafio consiste em criar uma aplicação utilizando a API do Rick and Morty

  

### Regras

*   Os dados devem ser consultados de [http://rickandmortyapi.com/](http://rickandmortyapi.com/)
*   Criar uma tela exibindo uma lista clicável com os nomes dos episódios 
*   Criar link para página com detalhes (conteúdo) do episódio, exibindo os personagens que participam daquele episódio
*   Qualquer IDE pode ser utilizada (recomendamos o [Visual Studio Code](https://code.visualstudio.com/))
  
### Diferenciais

*   Utilizar React, Angular ou .NET Core
*   Dividir código em camadas de negócio e acesso a dados
*   Implementar testes
*   Seguir o [SOLID](https://en.wikipedia.org/wiki/SOLID_(object-oriented_design)) e outros padrões considerados boas práticas de código

### Como entregar o código

*   Faça um Fork deste repositório (https://bitbucket.org/trilogo/web-developer-test/)
*   Crie uma Branch
*   Faça Commits a medida em que for trabalhando 
*   Quando finalizar, faça Push do código
*   Adicione o usuário hugo@trilogo.com.br ao seu repositório para que o código possa ser analisado